#! /bin/sh
cd $HOME
setbg &
# slidesbg 300 -r $HOME/Nextcloud/Pics &
dunst &
xcompmgr &
unclutter &
remaps &
mpd &
pulsemixer --set-volume 50 &
clipmenud &
doas trackpoint &
status-short &
emacs --daemon &
xxkb &
xcalib $HOME/.config/TPFLX.ICM &
xbacklight -set 20 &
# st -e opener &
dex -a -s $HOME/.config/autostart/ownCloud.desktop &
