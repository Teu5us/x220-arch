#!/bin/sh
setbg &
emacs --daemon --eval '(exwm-enable)'
dunst &
xcompmgr &
# xcompmgr -c -f -C -D 3 &
unclutter &
remaps &
mpd &
pulsemixer --set-volume 50 &
xbacklight -set 20 &
clipmenud &
doas trackpoint &
xcalib $HOME/.config/Lenovo_Thinkpad_X220_IPS_1366x768.icm &
xsetroot -cursor_name left_ptr &
xxkb &
xrdb ~/.Xresources &
exec emacsclient -c
