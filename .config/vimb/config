# Settings
set download-path=$HOME/Downloads/
set editor-command=st -t fst -e nvim %s
set font-size=14
set minimum-font-size=14
set history-max-items=30000
set home-page=https://duckduckgo.com
set smooth-scrolling=true
set spacial-navigation=true
set spell-checking=true
set spell-checking-languages=en,ru
set status-bar=false
set input-autohide=true
set webinspector=true
set frame-flattening=true
set accelerated-2d-canvas=true
set hardware-acceleration-policy=always
set hint-timeout=0
set incsearch=false
set media-playback-requires-user-gesture=true
set webgl=true
#set hint-follow-last=off
set stylesheet=false
set javascript-can-access-clipboard=false
set javascript-can-open-windows-automatically=false

# Handlers
handler-add magnet=st -e aria2c %s

# Remaps
set hint-keys=54321
nnoremap bo :open !
nnoremap ищ :open !
nnoremap bt :tabopen !
nnoremap ие :tabopen !
nnoremap q :quit<CR>
nnoremap й :quit<CR>
nnoremap Q :quit!<CR>
nnoremap Й :quit!<CR>
nnoremap sr :source ~/.config/vimb/config<CR>
nnoremap ык :source ~/.config/vimb/config<CR>
nmap H <C-o>
nmap Р <C-o>
nmap L <C-i>
nmap Д <C-i>
nmap ba :bma<Space>
nmap иф :bma<Space>
nmap br :bmr<Space>
nmap ик :bmr<Space>
nmap ss :set stylesheet!<CR>
nmap ыы :set stylesheet!<CR>
nmap жщ ;o
nmap же ;t
nmap жы ;s
nmap жЩ ;O
nmap жЕ ;T
nmap жу ;e
nmap жш ;i
nmap жШ ;I
nmap жз ;p
nmap жЗ ;P
nmap жч ;x

# Apps
## Dictionaries
### English
nmap dme :open multitran-english<Space>
nmap вьу :open multitran-english<Space>
nmap dca :open cambridge<Space>
nmap всф :open cambridge<Space>
nmap dfd :open freedictionary<Space>
nmap вав :open freedictionary<Space>
nmap dox :open oxford<Space>
nmap вщч :open oxford<Space>
nmap dco :open collins<Space>
nmap всщ :open collins<Space>
nmap dle :open linguee-english<Space>
nmap вду :open linguee-english<Space>
### German
nmap dmg :open multitran-german<Space>
nmap вьп :open multitran-german<Space>
nmap dlg :open linguee-german<Space>
nmap вдп :open linguee-german<Space>
## Wiki
nmap aaw :open arch-wiki<Space>
nmap ффц :open arch-wiki<Space>
## Law
nmap aco :open consultant<Space>
nmap фсщ :open consultant<Space>
nmap apr :open pravo-ru<Space>
nmap фзк :open pravo-ru<Space>
nmap aga :open garant<Space>
nmap фпф :open garant<Space>

# Apps in new window
## Dictionaries
### English
nmap Dme :tabopen multitran-english<Space>
nmap Вьу :tabopen multitran-english<Space>
nmap Dca :tabopen cambridge<Space>
nmap Всф :tabopen cambridge<Space>
nmap Dfd :tabopen freedictionary<Space>
nmap Вав :tabopen freedictionary<Space>
nmap Dox :tabopen oxford<Space>
nmap Вщч :tabopen oxford<Space>
nmap Dco :tabopen collins<Space>
nmap Всщ :tabopen collins<Space>
nmap Dle :tabopen linguee-english<Space>
nmap Вду :tabopen linguee-english<Space>
### German
nmap Dmg :tabopen multitran-german<Space>
nmap Вьп :tabopen multitran-german<Space>
nmap Dlg :tabopen linguee-german<Space>
nmap Вдп :tabopen linguee-german<Space>
## Wiki
nmap Aaw :tabopen arch-wiki<Space>
nmap Ффц :tabopen arch-wiki<Space>
## Law
nmap Aco :tabopen consultant<Space>
nmap Фсщ :tabopen consultant<Space>
nmap Apr :tabopen pravo-ru<Space>
nmap Фзк :tabopen pravo-ru<Space>
nmap Aga :tabopen garant<Space>
nmap Фпф :tabopen garant<Space>

# Shortcuts
## Dictionaries
### English
shortcut-add multitran-english=https://www.multitran.com/m.exe?l1=2&l2=1&s=$0
shortcut-add cambridge=https://dictionary.cambridge.org/dictionary/english/$0
shortcut-add freedictionary=https://www.thefreedictionary.com/$0
shortcut-add oxford=https://en.oxforddictionaries.com/definition/$0
shortcut-add collins=https://www.collinsdictionary.com/dictionary/english/$0
shortcut-add linguee-english=https://www.linguee.com/english-russian/search?query=$0
### German
shortcut-add multitran-german=https://www.multitran.com/m.exe?s=$0&l1=2&l2=3
shortcut-add linguee-german=https://www.linguee.com/english-german/search?query=$0
## Wiki
shortcut-add arch-wiki=https://wiki.archlinux.org/index.php?title=Special%%3ASearch&search=$0&go=Go
## Law
shortcut-add consultant=http://www.consultant.ru/search/?q=$0
shortcut-add pravo-ru=http://docs.pravo.ru/search/list/?type=list&search_query=$0
shortcut-add garant=http://ivo.garant.ru/#%2Fbasesearch%2F$0%2Fall%3A1

# Autocommands
## Stylesheet for rutor
aug rutor
	au LoadCommitted * set stylesheet=false
	au LoadCommitted *rutor* set stylesheet=true
aug end
## Go to normal mode after page is loaded
au LoadFinished * :normal

# Russian keys
nmap  ё `
nmap  ’ `
nmap  й q
nmap  ц w
nmap  у e
nmap  к r
nmap  е t
nmap  н y
nmap  г u
nmap  ш i
nmap  щ o
nmap  з p
nmap  х [
nmap  ъ ]
nmap  ї ]
nmap  ф a
nmap  ы s
nmap  і s
nmap  в d
nmap  а f
nmap  п g
nmap  р h
nmap  о j
nmap  л k
nmap  д l
nmap  ж ;
nmap  э '
nmap  є '
nmap  я z
nmap  ч x
nmap  с c
nmap  м v
nmap  и b
nmap  т n
nmap  ь m
nmap  б ,
nmap  ю .
nmap  Ё ~
nmap  Й Q
nmap  Ц W
nmap  У E
nmap  К R
nmap  Е T
nmap  Н Y
nmap  Г U
nmap  Ш I
nmap  Щ O
nmap  З P
nmap  Х {
nmap  Ъ }
nmap  Ї }
nmap  Ф A
nmap  Ы S
nmap  І S
nmap  В D
nmap  А F
nmap  П G
nmap  Р H
nmap  О J
nmap  Л K
nmap  Д L
nmap  Ж :
nmap  Э "
nmap  Є "
nmap  Я Z
nmap  Ч X
nmap  С C
nmap  М V
nmap  И B
nmap  Т N
nmap  Ь M
nmap  Б <
nmap  Ю >
nmap  ЯЯ ZZ
nmap  ЯЙ ZQ
nmap  йЖ q:
nmap  й. q/
nmap  й, q?
nmap  ёё ``
nmap  ёБ `<
nmap  ёЮ `>
nmap  ёх `[
nmap  ёъ `]
nmap  ёХ `{
nmap  ёЪ `}
nmap  ээ ''
nmap  эБ '<
nmap  эЮ '>
nmap  эх '[
nmap  эъ ']
nmap  эХ '{
nmap  эЪ '}
nmap  хэ ['
nmap  хё [`
nmap  хВ [D
nmap  хШ [I
nmap  хЗ [P
nmap  хх [[
nmap  хъ []
nmap  хс [c
nmap  хв [d
nmap  ха [f
nmap  хш [i
nmap  хь [m
nmap  хз [p
nmap  хы [s
nmap  хя [z
nmap  хХ [{
nmap  ъэ ]'
nmap  ъё ]`
nmap  ъВ ]D
nmap  ъШ ]I
nmap  ъЗ ]P
nmap  ъх ][
nmap  ъъ ]]
nmap  ъс ]c
nmap  ъв ]d
nmap  ъа ]f
nmap  ъш ]i
nmap  ъь ]m
nmap  ъз ]p
nmap  ъы ]s
nmap  ъя ]z
nmap  ъХ ]{
nmap  ьФ mA
nmap  ёФ `A
nmap  эФ 'A
nmap  пёФ g`A
nmap  пэФ g'A
nmap  ьИ mB
nmap  ёИ `B
nmap  эИ 'B
nmap  пёИ g`B
nmap  пэИ g'B
nmap  ьС mC
nmap  ёС `C
nmap  эС 'C
nmap  пёС g`C
nmap  пэС g'C
nmap  ьВ mD
nmap  ёВ `D
nmap  эВ 'D
nmap  пёВ g`D
nmap  пэВ g'D
nmap  ьУ mE
nmap  ёУ `E
nmap  эУ 'E
nmap  пёУ g`E
nmap  пэУ g'E
nmap  ьА mF
nmap  ёА `F
nmap  эА 'F
nmap  пёА g`F
nmap  пэА g'F
nmap  ьП mG
nmap  ёП `G
nmap  эП 'G
nmap  пёП g`G
nmap  пэП g'G
nmap  ьР mH
nmap  ёР `H
nmap  эР 'H
nmap  пёР g`H
nmap  пэР g'H
nmap  ьШ mI
nmap  ёШ `I
nmap  эШ 'I
nmap  пёШ g`I
nmap  пэШ g'I
nmap  ьО mJ
nmap  ёО `J
nmap  эО 'J
nmap  пёО g`J
nmap  пэО g'J
nmap  ьЛ mK
nmap  ёЛ `K
nmap  эЛ 'K
nmap  пёЛ g`K
nmap  пэЛ g'K
nmap  ьД mL
nmap  ёД `L
nmap  эД 'L
nmap  пёД g`L
nmap  пэД g'L
nmap  ьЬ mM
nmap  ёЬ `M
nmap  эЬ 'M
nmap  пёЬ g`M
nmap  пэЬ g'M
nmap  ьТ mN
nmap  ёТ `N
nmap  эТ 'N
nmap  пёТ g`N
nmap  пэТ g'N
nmap  ьЩ mO
nmap  ёЩ `O
nmap  эЩ 'O
nmap  пёЩ g`O
nmap  пэЩ g'O
nmap  ьЗ mP
nmap  ёЗ `P
nmap  эЗ 'P
nmap  пёЗ g`P
nmap  пэЗ g'P
nmap  ьЙ mQ
nmap  ёЙ `Q
nmap  эЙ 'Q
nmap  пёЙ g`Q
nmap  пэЙ g'Q
nmap  ьК mR
nmap  ёК `R
nmap  эК 'R
nmap  пёК g`R
nmap  пэК g'R
nmap  ьЫ mS
nmap  ёЫ `S
nmap  эЫ 'S
nmap  пёЫ g`S
nmap  пэЫ g'S
nmap  ьЕ mT
nmap  ёЕ `T
nmap  эЕ 'T
nmap  пёЕ g`T
nmap  пэЕ g'T
nmap  ьГ mU
nmap  ёГ `U
nmap  эГ 'U
nmap  пёГ g`U
nmap  пэГ g'U
nmap  ьМ mV
nmap  ёМ `V
nmap  эМ 'V
nmap  пёМ g`V
nmap  пэМ g'V
nmap  ьЦ mW
nmap  ёЦ `W
nmap  эЦ 'W
nmap  пёЦ g`W
nmap  пэЦ g'W
nmap  ьЧ mX
nmap  ёЧ `X
nmap  эЧ 'X
nmap  пёЧ g`X
nmap  пэЧ g'X
nmap  ьН mY
nmap  ёН `Y
nmap  эН 'Y
nmap  пёН g`Y
nmap  пэН g'Y
nmap  ьЯ mZ
nmap  ёЯ `Z
nmap  эЯ 'Z
nmap  пёЯ g`Z
nmap  пэЯ g'Z
nmap  ьф ma
nmap  ёф `a
nmap  эф 'a
nmap  пёф g`a
nmap  пэф g'a
nmap  ьи mb
nmap  ёи `b
nmap  эи 'b
nmap  пёи g`b
nmap  пэи g'b
nmap  ьс mc
nmap  ёс `c
nmap  эс 'c
nmap  пёс g`c
nmap  пэс g'c
nmap  ьв md
nmap  ёв `d
nmap  эв 'd
nmap  пёв g`d
nmap  пэв g'd
nmap  ьу me
nmap  ёу `e
nmap  эу 'e
nmap  пёу g`e
nmap  пэу g'e
nmap  ьа mf
nmap  ёа `f
nmap  эа 'f
nmap  пёа g`f
nmap  пэа g'f
nmap  ьп mg
nmap  ёп `g
nmap  эп 'g
nmap  пёп g`g
nmap  пэп g'g
nmap  ьр mh
nmap  ёр `h
nmap  эр 'h
nmap  пёр g`h
nmap  пэр g'h
nmap  ьш mi
nmap  ёш `i
nmap  эш 'i
nmap  пёш g`i
nmap  пэш g'i
nmap  ьо mj
nmap  ёо `j
nmap  эо 'j
nmap  пёо g`j
nmap  пэо g'j
nmap  ьл mk
nmap  ёл `k
nmap  эл 'k
nmap  пёл g`k
nmap  пэл g'k
nmap  ьд ml
nmap  ёд `l
nmap  эд 'l
nmap  пёд g`l
nmap  пэд g'l
nmap  ьь mm
nmap  ёь `m
nmap  эь 'm
nmap  пёь g`m
nmap  пэь g'm
nmap  ьт mn
nmap  ёт `n
nmap  эт 'n
nmap  пёт g`n
nmap  пэт g'n
nmap  ьщ mo
nmap  ёщ `o
nmap  эщ 'o
nmap  пёщ g`o
nmap  пэщ g'o
nmap  ьз mp
nmap  ёз `p
nmap  эз 'p
nmap  пёз g`p
nmap  пэз g'p
nmap  ьй mq
nmap  ёй `q
nmap  эй 'q
nmap  пёй g`q
nmap  пэй g'q
nmap  ьк mr
nmap  ёк `r
nmap  эк 'r
nmap  пёк g`r
nmap  пэк g'r
nmap  ьы ms
nmap  ёы `s
nmap  эы 's
nmap  пёы g`s
nmap  пэы g's
nmap  ье mt
nmap  ёе `t
nmap  эе 't
nmap  пёе g`t
nmap  пэе g't
nmap  ьг mu
nmap  ёг `u
nmap  эг 'u
nmap  пёг g`u
nmap  пэг g'u
nmap  ьм mv
nmap  ём `v
nmap  эм 'v
nmap  пём g`v
nmap  пэм g'v
nmap  ьц mw
nmap  ёц `w
nmap  эц 'w
nmap  пёц g`w
nmap  пэц g'w
nmap  ьч mx
nmap  ёч `x
nmap  эч 'x
nmap  пёч g`x
nmap  пэч g'x
nmap  ьн my
nmap  ён `y
nmap  эн 'y
nmap  пён g`y
nmap  пэн g'y
nmap  ья mz
nmap  ёя `z
nmap  эя 'z
nmap  пёя g`z
nmap  пэя g'z
nmap  ЭЭ ""
nmap  ЭЖ ":
nmap  Эю ".
nmap  ЭФ "A
nmap  ЭИ "B
nmap  ЭС "C
nmap  ЭВ "D
nmap  ЭУ "E
nmap  ЭА "F
nmap  ЭП "G
nmap  ЭР "H
nmap  ЭШ "I
nmap  ЭО "J
nmap  ЭЛ "K
nmap  ЭД "L
nmap  ЭЬ "M
nmap  ЭТ "N
nmap  ЭЩ "O
nmap  ЭЗ "P
nmap  ЭЙ "Q
nmap  ЭК "R
nmap  ЭЫ "S
nmap  ЭЕ "T
nmap  ЭГ "U
nmap  ЭМ "V
nmap  ЭЦ "W
nmap  ЭЧ "X
nmap  ЭН "Y
nmap  ЭЯ "Z
nmap  Эф "a
nmap  Эи "b
nmap  Эс "c
nmap  Эв "d
nmap  Эу "e
nmap  Эа "f
nmap  Эп "g
nmap  Эр "h
nmap  Эш "i
nmap  Эо "j
nmap  Эл "k
nmap  Эд "l
nmap  Эь "m
nmap  Эт "n
nmap  Эщ "o
nmap  Эз "p
nmap  Эй "q
nmap  Эк "r
nmap  Эы "s
nmap  Эе "t
nmap  Эг "u
nmap  Эм "v
nmap  Эц "w
nmap  Эч "x
nmap  Эн "y
nmap  Эя "z
nmap  пБ g<
nmap  пВ gD
nmap  пУ gE
nmap  пР gH
nmap  пШ gI
nmap  пО gJ
nmap  пТ gN
nmap  пЗ gP
nmap  пЙ gQ
nmap  пК gR
nmap  пЕ gT
nmap  пГ gU
nmap  пМ gV
nmap  пъ g]
nmap  пф ga
nmap  пв gd
nmap  пу ge
nmap  па gf
nmap  пА gF
nmap  пп gg
nmap  пр gh
nmap  пш gi
nmap  по gj
nmap  пл gk
nmap  пт gn
nmap  пь gm
nmap  пщ go
nmap  пз gp
nmap  пй gq
nmap  пк gr
nmap  пы gs
nmap  пе gt
nmap  пг gu
nmap  пм gv
nmap  пц gw
nmap  пч gx
nmap  пЁ g~
nmap  яю z.
nmap  яФ zA
nmap  яС zC
nmap  яВ zD
nmap  яУ zE
nmap  яА zF
nmap  яП zG
nmap  яР zH
nmap  яД zL
nmap  яЬ zM
nmap  яТ zN
nmap  яЩ zO
nmap  яК zR
nmap  яЦ zW
nmap  яЧ zX
nmap  яф za
nmap  яи zb
nmap  яс zc
nmap  яв zd
nmap  яу ze
nmap  яа zf
nmap  яп zg
nmap  яр zh
nmap  яш zi
nmap  яо zj
nmap  ял zk
nmap  яд zl
nmap  яь zm
nmap  ят zn
nmap  ящ zo
nmap  як zr
nmap  яы zs
nmap  яе zt
nmap  ям zv
nmap  яц zw
nmap  яч zx
nmap  яя zz
nmap  фЭ a"
nmap  шЭ i"
nmap  фэ a'
nmap  шэ i'
nmap  фБ a<
nmap  шБ i<
nmap  фЮ a>
nmap  шЮ i>
nmap  фИ aB
nmap  шИ iB
nmap  фЦ aW
nmap  шЦ iW
nmap  фх a[
nmap  шх i[
nmap  фъ a]
nmap  шъ i]
nmap  фё a`
nmap  шё i`
nmap  фи ab
nmap  ши ib
nmap  фз ap
nmap  шз ip
nmap  фы as
nmap  шы is
nmap  фе at
nmap  ше it
nmap  фц aw
nmap  шц iw
nmap  фХ a{
nmap  шХ i{
nmap  фЪ a}
nmap  шЪ i}
nmap  <C-W>Б <C-W><
nmap  <C-W>Ю <C-W>>
nmap  <C-W>Р <C-W>H
nmap  <C-W>О <C-W>J
nmap  <C-W>Л <C-W>K
nmap  <C-W>Д <C-W>L
nmap  <C-W>З <C-W>P
nmap  <C-W>К <C-W>R
nmap  <C-W>Ы <C-W>S
nmap  <C-W>Е <C-W>T
nmap  <C-W>Ц <C-W>W
nmap  <C-W>ъ <C-W>]
nmap  <C-W>и <C-W>b
nmap  <C-W>с <C-W>c
nmap  <C-W>в <C-W>d
nmap  <C-W>а <C-W>f
nmap  <C-W>А <C-W>F
nmap  <C-W>п <C-W>g
nmap  <C-W>пъ <C-W>g]
nmap  <C-W>пЪ <C-W>g}
nmap  <C-W>па <C-W>gf
nmap  <C-W>пА <C-W>gF
nmap  <C-W>р <C-W>h
nmap  <C-W>ш <C-W>i
nmap  <C-W>о <C-W>j
nmap  <C-W>л <C-W>k
nmap  <C-W>д <C-W>l
nmap  <C-W>т <C-W>n
nmap  <C-W>щ <C-W>o
nmap  <C-W>з <C-W>p
nmap  <C-W>й <C-W>q
nmap  <C-W>к <C-W>r
nmap  <C-W>ы <C-W>s
nmap  <C-W>е <C-W>t
nmap  <C-W>м <C-W>v
nmap  <C-W>ц <C-W>w
nmap  <C-W>ч <C-W>x
nmap  <C-W>я <C-W>z
nmap  <C-W>Ъ <C-W>}
