#!/bin/sh
$HOME/.monicarc   #monitor calibration

[ -f ~/.Xmodmap ] && xmodmap ~/.Xmodmap
[ -f ~/.Xresources ] && xrdb -merge ~/.Xresources

xxkb &
xset r rate 200 50

exec i3
